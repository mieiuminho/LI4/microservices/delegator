#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import Flask
from delegator import delegator

__version__ = "0.1.0"

app = Flask(__name__)


@app.route("/")
def index():
    return {"name": "delegator", "version": __version__}


@app.route("/run")
def run():
    delegator()
    return {"message": "Ok"}, 200


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)

[hugo]: https://github.com/HugoCarvalho99
[hugo-pic]: https://github.com/HugoCarvalho99.png?size=120
[nelson]: https://github.com/nelsonmestevao
[nelson-pic]: https://github.com/nelsonmestevao.png?size=120
[pedro]: https://github.com/pedroribeiro22
[pedro-pic]: https://github.com/pedroribeiro22.png?size=120
[celso]: https://github.com/Basto97
[celso-pic]: https://github.com/Basto97.png?size=120
[ricardo]: https://github.com/ricardoslv
[ricardo-pic]: https://github.com/ricardoslv.png?size=120

<div align="center">
    <img src="static/logo.png" alt="delegatewise" width="350px">
</div>

## :rocket: Getting Started

For data persistence this project uses a MongoDB database. You should have
MongoDB up and running. Start by running the setup script.

```bash
bin/setup
```

It creates a `.env` file that you should check if it needs any manual change.

### :inbox_tray: Prerequisites

The following software is required to be installed on your system:

- [Python](https://www.python.org/downloads/)

### :hammer: Development

Start a development server and visit <https://localhost:5000/>.

```
bin/server
```

Format the code accordingly to common guide lines.

```
bin/format
```

Lint your python code.

```
bin/lint
```

### :package: Deployment

Deploy the application to Heroku.

```
bin/deploy
```

## :busts_in_silhouette: Team

| [![Hugo][hugo-pic]][hugo] | [![Nelson][nelson-pic]][nelson] | [![Pedro][pedro-pic]][pedro] | [![Celso][celso-pic]][celso] | [![Ricardo][ricardo-pic]][ricardo] |
| :-----------------------: | :-----------------------------: | :--------------------------: | :--------------------------: | :--------------------------------: |
|   [Hugo Carvalho][hugo]   |    [Nelson Estevão][nelson]     |    [Pedro Ribeiro][pedro]    |   [Celso Rodrigues][celso]   |      [Ricardo Silva][ricardo]      |

from pymongo import MongoClient
from ortools.linear_solver import pywraplp
from bson.objectid import ObjectId
import os


def parseVar(s):
    excludeX = s[1:]
    separate = excludeX.split("s")
    return (separate[0], separate[1])


def generateVarName(memberId, taskId):
    return 'X' + memberId + 's' + taskId


def makeUserExpression(info, memberId, memberWorkload):
    (wsInit, idInit) = info[0]

    exp = str(wsInit) + "*" + generateVarName(memberId, idInit)
    for i in range(1, len(info)):
        (wl, Id) = info[i]
        exp += "+" + str(wl) + "*" + generateVarName(memberId, Id)
    exp += '+' + str(memberWorkload)
    return exp


def makeSumExpression(elems, value):
    if (len(elems) >= 1):
        exp = "solver.Add(" + elems[0]
        for i in range(1, len(elems)):
            exp = exp + '+' + elems[i]
        exp = exp + "==" + str(value) + ")"
        return exp
    return ''


def solver(ws, db):

    solver = pywraplp.Solver('simple_mip_program',
                             pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING)
    taskInfo = []
    vars = []

    for taskId in ws["tasks"]:

        task = ws['tasks'][taskId]

        assocSkill = task['skillSet']
        taskInfo.append((task['workload'], taskId))

        oneSumVars = []
        zeroSumVars = []

        for memberId in ws["members"]:

            varName = generateVarName(memberId, task['_id'])

            exec(varName + "= solver.IntVar(0.0,solver.infinity(),'" +
                 varName + "')")

            vars.append(varName)

            if not assocSkill or assocSkill in ws['userSkills'][memberId]:
                oneSumVars.append(varName)
            else:
                zeroSumVars.append(varName)

        buf = makeSumExpression(oneSumVars, 1)
        exec(buf)
        buf = makeSumExpression(zeroSumVars, 0)
        exec(buf)

    lastMaxMin = 1

    if (len(ws['members']) > 1):

        firstMemberId = ws['members'][0]
        firstMemberWorkload = ws['userWorkloads'][firstMemberId]
        secondMemberId = ws['members'][1]
        secondMemberWorkload = ws['userWorkloads'][secondMemberId]

        fstExp = makeUserExpression(taskInfo, firstMemberId,
                                    firstMemberWorkload)
        sndExp = makeUserExpression(taskInfo, secondMemberId,
                                    secondMemberWorkload)

        exec("Min1=solver.Var(0.0,solver.infinity(),False,'Min1')")
        exec("Max1=solver.Var(0.0,solver.infinity(),False, 'Max1')")
        exec("b1=solver.IntVar(0.0,solver.infinity(),'b1')")
        exec("nb1=solver.IntVar(0.0,solver.infinity(),'nb1')")

        exec("solver.Add(Min1 <=" + fstExp + ')')
        exec("solver.Add(Min1 <=" + sndExp + ')')
        exec("solver.Add(Max1 >=" + fstExp + ')')
        exec("solver.Add(Max1 >=" + sndExp + ')')
        exec("solver.Add(Max1 <=" + fstExp + '+100000*b1)')
        exec("solver.Add(Max1 <=" + sndExp + '+100000*nb1)')
        exec("solver.Add(b1 + nb1 == 1)")

        for i in range(2, len(ws['members'])):
            currentMax = 'Max' + str(lastMaxMin + 1)
            currentMin = 'Min' + str(lastMaxMin + 1)
            currentB = 'b' + str(lastMaxMin + 1)
            currentNB = 'nb' + str(lastMaxMin + 1)

            firstMemberId = ws['members'][i]
            firstMemberWorkload = ws['userWorkloads'][firstMemberId]

            fstExp = makeUserExpression(taskInfo, firstMemberId,
                                        firstMemberWorkload)
            sndExp = 'Min' + str(lastMaxMin)

            exec(currentMin + "=solver.Var(0.0,solver.infinity(),False,'" +
                 currentMin + "')")
            exec(currentMax + "=solver.Var(0.0,solver.infinity(),False, '" +
                 currentMax + "')")
            exec(currentB + "=solver.IntVar(0.0,solver.infinity(),'" +
                 currentB + "')")
            exec(currentNB + "=solver.IntVar(0.0,solver.infinity(),'" +
                 currentNB + "')")
            exec("solver.Add(" + currentMin + " <=" + fstExp + ')')
            exec("solver.Add(" + currentMin + " <=" + sndExp + ')')
            sndExp = 'Max' + str(lastMaxMin)

            exec("solver.Add(" + currentMax + " >=" + fstExp + ')')
            exec("solver.Add(" + currentMax + " >=" + sndExp + ')')
            exec("solver.Add(" + currentMax + " <=" + fstExp + '+100000*' +
                 currentB + ')')
            exec("solver.Add(" + currentMax + " <=" + sndExp + '+100000*' +
                 currentNB + ')')
            exec("solver.Add(" + currentB + "+" + currentNB + "==1)")

            lastMaxMin += 1

    lastMax = 'Max' + str(lastMaxMin)
    lastMin = 'Min' + str(lastMaxMin)
    exec("solver.Minimize(" + lastMax + "-" + lastMin + ")")

    status = solver.Solve()
    print(ws['_id'], status)

    modifiedTasks = ws['tasks']

    for s in vars:
        (userId, taskId) = parseVar(s)
        objectUserId = ObjectId(userId)
        modifiedUsers = db.users.find({'_id': objectUserId})[0]['tasks']
        exec('varValue=' + s + '.solution_value()')
        exec(
            "if varValue == 1.0:\n\tmodifiedTasks[taskId]['assignee'] = userId\n\tmodifiedUsers[str(ws['_id'])].append(taskId)"
        )
        db.users.update_one({'_id': objectUserId},
                            {'$set': {
                                'tasks': modifiedUsers
                            }})

    db.workspaces.update_one({'_id': ws['_id']},
                             {'$set': {
                                 'tasks': modifiedTasks
                             }})


def delegator():
    db_name = os.environ['DELEGATE_DATA_MONGODB_DB']
    uri = os.environ['DELEGATE_DATA_MONGODB_URI']

    db = MongoClient(uri)[db_name]

    workspaces = db.workspaces.find({})

    for ws in workspaces:
        if len(ws['members']) > 0 and len(ws['tasks']) > 0:
            solver(ws, db)


if __name__ == '__main__':
    delegator()
